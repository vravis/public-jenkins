#!/bin/bash
echo "#################Building the image for  #################"
BUILD_NAME="hw-jenkins"
docker build -t $BUILD_NAME hw-jenkins/.
CHK="$?"
if [ "$CHK" -ne 0 ];then
    echo "##################Build failed####################"
    exit 3
fi
    echo "################## TAG IMAGE####################"
docker tag $BUILD_NAME localhost:5000/$BUILD_NAME

if [ "$CHK" -ne 0 ];then
    echo "################## TAG failed####################"
    exit 3
fi

    echo "################## PUSH IMAGE###################"
docker push localhost:5000/$BUILD_NAME

if [ "$CHK" -ne 0 ];then
    echo "################## PUSH failed####################"
    exit 3
fi

